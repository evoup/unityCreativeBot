using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadWebPicScript : MonoBehaviour {


	public void loadBgImage() {
		StartCoroutine(RealLoadImage());
	}

    public void loadAdLogoImage()
    {
        StartCoroutine(RealLogoImage());
    }

    public void loadRoleImage() {
        StartCoroutine(RealRoleImage());
    }

	//This is where the actual code is executed
	private IEnumerator RealLoadImage () {

		//A URL where the image is stored
        string url = "http://172.16.25.145:8080/api/performad/material/m/26401";
        Dictionary<string, string> headers = new Dictionary<string, string>();
        //headers.Add("server-call", "adsapi");
        //Call the WWW class constructor
        WWW imageURLWWW = new WWW(url, null, headers);

		//Wait for the download
		yield return imageURLWWW;

        GameObject obj = GameObject.Find("BgImage");
        obj.transform.localScale = new Vector3(1, 1, 1);

        //Simple check to see if there's indeed a texture available
        if (imageURLWWW.texture != null) {

			//Construct a new Sprite
			Sprite sprite = new Sprite();

            //Create a new sprite using the Texture2D from the url. 
            //Note that the 400 parameter is the width and height. 
            //Adjust accordingly
            sprite = Sprite.Create(imageURLWWW.texture, new Rect(0, 0, imageURLWWW.texture.width, imageURLWWW.texture.height), Vector2.zero);  

			//Assign the sprite to the Image Component
			//GetComponent<UnityEngine.UI.Image>().sprite = sprite;  
			GameObject image = GameObject.Find("BgImage");
            image.GetComponent<Image>().sprite = sprite;
		}

		yield return null;
	}


    private IEnumerator RealLogoImage()
    {

        //A URL where the image is stored
        string url = "http://172.16.25.145:8080/api/performad/material/m/26361";
        Dictionary<string, string> headers = new Dictionary<string, string>();
        //headers.Add("server-call", "adsapi");
        //Call the WWW class constructor
        WWW imageURLWWW = new WWW(url, null, headers);

        //Wait for the download
        yield return imageURLWWW;

        GameObject obj = GameObject.Find("LogoImage");
        obj.transform.localScale = new Vector3(1, 1, 1);

        //Simple check to see if there's indeed a texture available
        if (imageURLWWW.texture != null)
        {

            //Construct a new Sprite
            Sprite sprite = new Sprite();

            //Create a new sprite using the Texture2D from the url. 
            //Note that the 400 parameter is the width and height. 
            //Adjust accordingly
            sprite = Sprite.Create(imageURLWWW.texture, new Rect(0, 0, imageURLWWW.texture.width, imageURLWWW.texture.height), Vector2.zero);

            //Assign the sprite to the Image Component
            //GetComponent<UnityEngine.UI.Image>().sprite = sprite;  
            GameObject image = GameObject.Find("LogoImage");
            image.GetComponent<Image>().sprite = sprite;
        }

        yield return null;
    }

    private IEnumerator RealRoleImage()
    {

        //A URL where the image is stored
        string url = "http://172.16.25.145:8080/api/performad/material/m/26402";
        Dictionary<string, string> headers = new Dictionary<string, string>();
        //headers.Add("server-call", "adsapi");
        //Call the WWW class constructor
        WWW imageURLWWW = new WWW(url, null, headers);

        //Wait for the download
        yield return imageURLWWW;

        GameObject obj = GameObject.Find("RoleImage");
        obj.transform.localScale = new Vector3(1, 1, 1);

        //Simple check to see if there's indeed a texture available
        if (imageURLWWW.texture != null)
        {

            //Construct a new Sprite
            Sprite sprite = new Sprite();

            //Create a new sprite using the Texture2D from the url. 
            //Note that the 400 parameter is the width and height. 
            //Adjust accordingly
            sprite = Sprite.Create(imageURLWWW.texture, new Rect(0, 0, imageURLWWW.texture.width, imageURLWWW.texture.height), Vector2.zero);

            //Assign the sprite to the Image Component
            //GetComponent<UnityEngine.UI.Image>().sprite = sprite;  
            GameObject image = GameObject.Find("RoleImage");
            image.GetComponent<Image>().sprite = sprite;
        }

        yield return null;
    }
}