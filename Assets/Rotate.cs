﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {
	public float rotationSpeed = 10.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float yAngle = rotationSpeed * Time.deltaTime;
		transform.Rotate(0.0f, yAngle, 0.0f);
	}
}
