using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class Upload : MonoBehaviour {

	public void StartUpload() {
		StartCoroutine(UploadFile());
	}




	IEnumerator UploadFile0() {
        var form = new WWWForm();
        form.AddField("type", "0");
        form.AddField("advertiserId", "128");
        string filePath0 = "C:\\projects\\图片素材\\前景\\主角无名女.jpg";
        byte[] bytes = File.ReadAllBytes(filePath0);
        form.AddBinaryData("f1", bytes, "image.jpg", "image/jpeg");
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("server-call", "adsapi");
        headers.Add("Content-Type", "multipart/form-data; boundary=\"rcHvSXGSyKCnJnWUc0aIiifrgTycfyP5ESw8x5q5\"");
        WWW w = new WWW("http://172.16.25.145:8080/api/performad/material/upload", form.data, headers);
        yield return w;
	}


    IEnumerator UploadFile()
    {
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection("type", "0"));
        formData.Add(new MultipartFormDataSection("advertiserId", "128"));
        string filePath = "C:\\projects\\图片素材\\前景\\主角无名女.jpg";
        byte[] imageData = File.ReadAllBytes(filePath);
        formData.Add(new MultipartFormFileSection("f1", imageData, "主角无名女.jpg", "image/jpeg"));
        //formData.Add(new MultipartFormDataSection("--MyBoundary--\r\n"));
        UnityWebRequest www = UnityWebRequest.Post("http://172.16.25.145:8080/api/performad/material/upload", formData);
        www.SetRequestHeader("server-call", "adsapi");
        Debug.Log("1");
        yield return www.SendWebRequest();
        //if(www.isNetworkError || www.isHttpError) {
        if (www.responseCode == 200) {
            Debug.Log("Form upload complete!");
        } else {
            Debug.Log(www.responseCode);
            Debug.Log(">>>----------");
            Debug.Log("----------<<<");
            Debug.Log(www.error);
        }
    }
}