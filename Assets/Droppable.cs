﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Droppable : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    // 释放区域中所显示的物体
    private Image iconImage;
    // 释放区域中所显示物体的高亮颜色
    private Color highlightedColor = Color.black;
    // 保存释放区域中所显示物体的原本颜色
    private Color normalColor;
    public void OnDrop(PointerEventData pointerEventData)
    {
        // 获取已拖拽的物体的Image组件
        Image droppedImage = pointerEventData.pointerDrag.GetComponent<Image>();
        // 将释放区域中已显示的物体的精灵
        // 变为与被释放物体相同的精灵，颜色还原
        iconImage.sprite = droppedImage.sprite;
        //iconImage.color = normalColor;
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        if (pointerEventData.dragging)
        {
            // 如果在拖拽过程中的话，释放区域中所显示的物体颜色变为高亮颜色
            iconImage.color = highlightedColor;
        }
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        if (pointerEventData.dragging)
        {
            // 如果在拖拽过程中的话，释放区域中显示的物体颜色还原为原本颜色
           // iconImage.color = normalColor;
        }
    }

    void Start()
    {
       // normalColor = iconImage.color;
    }
}
