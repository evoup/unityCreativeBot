﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scale : MonoBehaviour {

    GameObject rotater;

    void Start()
    {
        rotater = GameObject.Find("RoleImage");
    }
    public void btnChangeHeight()
    {
        rotater.gameObject.transform.localScale += new Vector3(0, 0.01f, 0);
    }

    public void btnChangeWidth()
    {
        rotater.gameObject.transform.localScale += new Vector3(0.01f, 0, 0);
    }

    public void btnChangeHeightM()
    {
        rotater.gameObject.transform.localScale -= new Vector3(0, 0.01f, 0);
    }

    public void btnChangeWidthM()
    {
        rotater.gameObject.transform.localScale -= new Vector3(0.01f, 0, 0);
    }
}
